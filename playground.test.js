const fsPromises = require('fs').promises;
const path = require('path');
const JSDO = require('./index');

let jsdo;

beforeAll(async () => {
  const jsonSchemaDir = path.join(__dirname, '__tests__/schemas');

  try {
    const schemaFilenames = await fsPromises.readdir(jsonSchemaDir);
    const schemaFilePaths = schemaFilenames.map(sf => `${jsonSchemaDir}/${sf}`);
    const schemas = await Promise.all(
      schemaFilePaths.map(async sp => JSON.parse(await fsPromises.readFile(sp))),
    );
    jsdo = new JSDO({ schemas });

    const dereferedStruct = jsdo.derefSchema('struct');
    console.info(dereferedStruct);
  } catch (err) {
    console.error(err);
  }
});

test('playground', () => {
  expect(true).toEqual(true);
});
