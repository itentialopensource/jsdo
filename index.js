const R = require('ramda');

/**
 * A JSON schema dereference operator
 */
class JSDO {
  /**
   * Creates a JSON schema dereference operator.
   * @param {Array} schemas — An initial list of schemas
   */
  constructor({ schemas }) {
    this.schemas = schemas.reduce(
      (ss, s) => ({
        ...ss,
        // Remove definitions from schemas inside of schemas list
        [s.$id]: JSDO.sanitizeDefintions(s),
        // Add schema definitions to schemas list if available
        ...(s.definitions &&
          Object.keys(s.definitions).reduce((ds, dk) => {
            // Convert definition into reference within scope of parent
            const def = s.definitions[dk];
            const id = def.$id ? `${s.$id}#${dk}` : `${s.$id}#/definitions/${dk}`;

            return {
              ...ds,
              [id]: def,
            };
          }, {})),
      }),
      {},
    );
  }

  /**
   * Converts a JSON pointer into an array of segments called a path.
   * @param {string} pointer — json pointer
   * @returns {Array} path
   */
  static convertJsonPointerToPath(pointer) {
    const path = pointer.split('/').slice(1);

    return path.map(p => {
      const potentialIndex = Number.parseInt(p, 10);
      const isNan = Number.isNaN(potentialIndex);

      return isNan ? p : potentialIndex;
    });
  }

  /**
   * Removes any $id and $ref from a schema given a path.
   * @param {Array} path
   * @param {Object} schema
   * @returns {Object} sanitized schema
   */
  static sanitizeDerefOp(path, schema) {
    const pathToId = [...path, '$id'];
    const pathToRef = [...path, '$ref'];
    return R.pipe(R.dissocPath(pathToId), R.dissocPath(pathToRef))(schema);
  }

  /**
   * Removes definitions property from schema
   * @param {Object} schema
   * @returns {Object} sanitized schema
   */
  static sanitizeDefintions(schema) {
    return R.dissocPath(['definitions'])(schema);
  }

  /**
   * Checks if a value is an object and not an array or null. This is necessary due to
   * the limitation of [typeof](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/typeof)
   * @param {*} val
   * @returns {boolean}
   */
  static isObject(val) {
    return Object.prototype.toString.call(val) === '[object Object]';
  }

  /**
   * Finds any `$ref` properties inside of a schema object.
   * @param {Object} schema
   * @param {string} [pointer='']
   * @param {Array} [refs=[]]
   * @returns {Array} JSON pointers
   */
  findRefs(schema, pointer = '', refs = []) {
    return Object.keys(schema).reduce((rs, prop) => {
      const node = schema[prop];
      const isObject = JSDO.isObject(node);
      const isArray = Array.isArray(node);

      if (isObject) {
        // if property is an object, recursively check its child properties
        return this.findRefs(node, `${pointer}/${prop}`, rs);
      }

      if (isArray) {
        // if property is an array, recursively check its child properties
        return R.pipe(
          n => n.map((childN, i) => this.findRefs(childN, `${pointer}/${prop}/${i}`, rs)),
          R.flatten,
          R.uniq,
        )(node);
      }

      if (prop === '$ref') {
        // if `$ref` property is found, return the corresponding JSON pointer.
        return [...rs, pointer];
      }
      return rs;
    }, refs);
  }

  /**
   * Recursively finds and dereferences all subschemas
   * @param {Object} schema — the schema being processed for dereference
   * @param {Array} derefOps — a record of all dereference operations
   *                           so that recursive references are prevented from being processed
   * @returns {Object} dereferenced schema object
   */
  recursiveDeref(schema, derefOps = [{ id: schema.$id, pointer: '/' }]) {
    const refPointers = this.findRefs(schema);

    if (refPointers.length > 0) {
      const refPaths = refPointers.map(r => JSDO.convertJsonPointerToPath(r));

      const refs = refPaths
        .map((refPath, i) => {
          /**
           * Maps reference paths to an object containing the ref id, path, lens and subschema.
           * @param {Array} refPath
           * @returns {Object} ref — contains id, path, lens and subschema for a reference.
           */
          const refLens = R.lensPath(refPath);
          const refId = R.view(refLens, schema).$ref;
          const refPointer = refPointers[i];
          const isSelfAnchor = refId === '#';
          const isAnchor = refId[0] === '#';

          let absoluteRefId;
          if (isSelfAnchor) {
            absoluteRefId = schema.$id;
          } else if (isAnchor) {
            /*

             */
            /*
            Check derefops to see if any ops include the pointer:
            If ops with included parent pointer exists, take id and pull out root id
            Else use the root schema ID

            This is for handling anchor references that are contained within a subschema
            which has already been dereferenced previously.
            Without this logic, the relationship to the parent schema is lost.
             */
            const parentDerefOp = derefOps.find(
              op => op.pointer !== '/' && refPointer.includes(op.pointer),
            );

            if (parentDerefOp && parentDerefOp.id) {
              const [parentSchemaId] = parentDerefOp.id.split('#');
              absoluteRefId = `${parentSchemaId}${refId}`;
            } else {
              absoluteRefId = `${schema.$id}${refId}`;
            }
          } else {
            absoluteRefId = refId;
          }

          return {
            id: absoluteRefId,
            pointer: refPointer,
            path: refPath,
            lens: refLens,
            subschema: this.schemas[absoluteRefId],
          };
        })
        .reduce((rs, r) => {
          /**
           * Removes references that could cause a recursive dereference operation
           * to prevent being stuck in an infinite loop.
           * @param {Array} rs — multiple ref's
           * @param {Object} r — current ref
           * @param {number} i — current ref index
           * @returns {Array} rs — multiple ref's with recursive references removed
           */
          const isRecursiveRef = derefOps.find(
            o => !!(o.id === r.id && r.pointer.includes(o.pointer)),
          );

          if (!isRecursiveRef) {
            return [...rs, r];
          }
          return rs;
        }, []);

      const derefedSchema = refs.reduce((s, ref) => {
        /**
         * Processes dereference operation and sanitizes dereferenced schema.
         * @returns {Object} schema
         */
        const derefed = R.set(ref.lens, ref.subschema, s);

        derefOps.push({ id: ref.id, pointer: ref.pointer });

        return derefed;
      }, schema);

      const sanitizedSchema = refPaths.reduce((d, p) => JSDO.sanitizeDerefOp(p, d), derefedSchema);

      return this.recursiveDeref(sanitizedSchema, derefOps);
    }
    return schema;
  }

  /**
   * Adds schema to object's schema list after initialization.
   * This is useful for situation where the schemas are added iteratively instead of known up-front.
   * @param {string} id
   * @param {Object} schema
   */
  addSchema(id, schema) {
    this.schemas = {
      ...this.schemas,
      [id]: schema,
    };
  }

  /**
   * Dereferences a schema based on its ID inside of an JSDO object's schema list.
   * @param {string} id — a schema ID
   * @returns {Object} schema — a dereferenced schema
   */
  derefSchema(id) {
    const schema = this.schemas[id];

    if (!schema) {
      return null;
    }
    return this.recursiveDeref(schema);
  }
}

module.exports = JSDO;
