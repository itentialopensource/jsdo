const fsPromises = require('fs').promises;
const path = require('path');
const JSDO = require('..');

let jsdo;

beforeAll(async () => {
  const jsonSchemaDir = path.join(__dirname, 'schemas');

  try {
    const schemaFilenames = await fsPromises.readdir(jsonSchemaDir);
    const schemaFilePaths = schemaFilenames.map(sf => `${jsonSchemaDir}/${sf}`);
    const schemas = await Promise.all(
      schemaFilePaths.map(async sp => JSON.parse(await fsPromises.readFile(sp))),
    );
    jsdo = new JSDO({ schemas });
  } catch (err) {
    console.error(err);
  }
});

test('JSDO.convertJsonPointerToPath', () => {
  expect(JSDO.convertJsonPointerToPath('/node/leaf')).toEqual(['node', 'leaf']);
  expect(JSDO.convertJsonPointerToPath('/node/leaf/0')).toEqual(['node', 'leaf', 0]);
});

test('JSDO.sanitizeDerefOp', () => {
  const schema = {
    type: 'object',
    properties: {
      name: {
        type: 'string',
      },
    },
  };
  const definitions = {
    definitions: {
      date: {
        type: 'date-time',
      },
    },
  };
  const schemaWithDefinitions = {
    ...schema,
    ...definitions,
  };

  expect(JSDO.sanitizeDefintions(schemaWithDefinitions)).toEqual(schema);
  expect(JSDO.sanitizeDefintions(schemaWithDefinitions)).not.toHaveProperty('definitions');
});

test('JSDO.isObject', () => {
  expect(JSDO.isObject({})).toBe(true);
  expect(JSDO.isObject([])).toBe(false);
  expect(JSDO.isObject(null)).toBe(false);
});

test('findRefs', () => {
  const result = [
    '/properties/propFromDef',
    '/properties/propFromIdDef',
    '/properties/selfRef',
    '/properties/leafsAsObject/properties/create',
    '/properties/leafsAsObject/properties/read',
    '/properties/leafsAsObject/properties/update',
    '/properties/leafsAsObject/properties/delete',
    '/properties/leafsAsArray/items',
    '/properties/leafsAsTuple/items/0',
  ];
  expect(jsdo.findRefs(jsdo.schemas.node)).toEqual(result);
});

test('addSchema', () => {
  const fakeSchema = {
    $id: 'fake',
    type: 'object',
    properties: {
      foo: {
        type: 'boolean',
      },
      bar: {
        type: 'number',
      },
    },
  };

  jsdo.addSchema(fakeSchema.$id, fakeSchema);

  expect(jsdo.schemas.fake).toEqual(fakeSchema);
  expect(jsdo.derefSchema('fake')).toEqual(fakeSchema);
});

test('derefSchema', () => {
  const result = {
    $id: 'node',
    properties: {
      leafsAsArray: {
        items: {
          properties: {
            children: {},
            meta: {
              properties: { date: { type: 'date-time' }, description: { type: 'string' } },
              type: 'object',
            },
            name: { type: 'string' },
          },
          type: 'object',
        },
        type: 'array',
      },
      leafsAsObject: {
        properties: {
          create: {
            properties: {
              children: {},
              meta: {
                properties: { date: { type: 'date-time' }, description: { type: 'string' } },
                type: 'object',
              },
              name: { type: 'string' },
            },
            type: 'object',
          },
          delete: {
            properties: {
              children: {},
              meta: {
                properties: { date: { type: 'date-time' }, description: { type: 'string' } },
                type: 'object',
              },
              name: { type: 'string' },
            },
            type: 'object',
          },
          read: {
            properties: {
              children: {},
              meta: {
                properties: { date: { type: 'date-time' }, description: { type: 'string' } },
                type: 'object',
              },
              name: { type: 'string' },
            },
            type: 'object',
          },
          update: {
            properties: {
              children: {},
              meta: {
                properties: { date: { type: 'date-time' }, description: { type: 'string' } },
                type: 'object',
              },
              name: { type: 'string' },
            },
            type: 'object',
          },
        },
        type: 'object',
      },
      leafsAsTuple: {
        items: [
          {
            properties: {
              children: {},
              meta: {
                properties: { date: { type: 'date-time' }, description: { type: 'string' } },
                type: 'object',
              },
              name: { type: 'string' },
            },
            type: 'object',
          },
          { type: 'integer' },
        ],
        type: 'array',
      },
      name: { type: 'string' },
      propFromDef: { type: 'string' },
      propFromIdDef: { type: 'number' },
      selfRef: {},
    },
    type: 'object',
  };

  expect(jsdo.derefSchema('node')).toEqual(result);
});

test('derefSchema unknown schema', () => {
  expect(jsdo.derefSchema('unknownSchema')).toEqual(null);
});

test('derefSchema preserve derefed parent subschema', () => {
  expect(jsdo.derefSchema('struct')).toEqual({
    $id: 'struct',
    type: 'object',
    properties: {
      nodeId: { type: 'string', examples: ['kdWO-g4oS3GkzrbqDqCZUw'] },
      type: { type: 'string', const: 'array' },
      items: {
        type: 'array',
        items: {
          anyOf: [
            {
              type: 'object',
              properties: {
                nodeId: {},
                type: { type: 'string', const: 'array' },
                title: { type: 'string', examples: ['Connection Information'] },
                description: {
                  type: 'string',
                  examples: ['This section describes the connection information for a device.'],
                },
                items: { type: 'array', items: { anyOf: [{}, {}, {}, {}, {}, {}] } },
              },
              required: ['nodeId', 'type', 'title', 'description', 'items'],
              additionalProperties: false,
            },
            {
              type: 'object',
              properties: {
                nodeId: {},
                type: { type: 'string', const: 'boolean' },
                title: { type: 'string', examples: ['Allow timeout'] },
                description: {
                  type: 'string',
                  examples: ['Is a timeout allowed to pass this step?'],
                },
                default: { type: 'boolean', examples: [false] },
              },
              required: ['nodeId', 'type', 'title', 'description'],
              additionalProperties: false,
            },
            {
              type: 'object',
              properties: {
                nodeId: {},
                type: { type: 'string', const: 'number' },
                widget: { type: 'string', const: 'updown' },
                title: { type: 'string', examples: ['Retry attempts'] },
                description: {
                  type: 'string',
                  examples: ['The number of connection retries before timing out.'],
                },
                placeholder: { type: 'string', examples: ['Enter connection retry attempt count'] },
                required: { type: 'boolean' },
                validation: { type: 'object' },
              },
              required: ['nodeId', 'type', 'title', 'description', 'placeholder', 'required'],
              additionalProperties: false,
            },
            {
              type: 'object',
              properties: {
                nodeId: {},
                type: { type: 'string', const: 'string' },
                title: { type: 'string', examples: ['Device Name'] },
                description: { type: 'string', examples: ['A unique name of the target device.'] },
                placeholder: { type: 'string', examples: ["Enter the device's name"] },
                required: { type: 'boolean' },
                pattern: { type: 'string', examples: ['regex'] },
                validation: { type: 'object' },
              },
              required: ['nodeId', 'type', 'title', 'description', 'placeholder', 'required'],
              additionalProperties: false,
            },
            {
              type: 'object',
              properties: {
                nodeId: {},
                type: { type: 'string', const: 'string' },
                widget: { type: 'string', const: 'textarea' },
                title: { type: 'string', examples: ['Device Name'] },
                description: { type: 'string', examples: ['A unique name of the target device.'] },
                placeholder: { type: 'string', examples: ["Enter the device's name"] },
                required: { type: 'boolean' },
                pattern: { type: 'string', examples: ['regex'] },
                validation: { type: 'object' },
              },
              required: ['nodeId', 'type', 'title', 'description', 'placeholder', 'required'],
              additionalProperties: false,
            },
            {
              type: 'object',
              properties: {
                nodeId: { $ref: '#/definitions/nodeId' },
                type: { type: 'string', const: 'string' },
                title: { type: 'string', examples: ['Device Name'] },
                description: { type: 'string', examples: ['A unique name of the target device.'] },
                placeholder: { type: 'string', examples: ["Enter the device's name"] },
                required: { type: 'boolean' },
                enum: { type: 'array', default: [] },
                binding: { type: 'boolean', default: false },
                pattern: { type: 'string', examples: ['regex'] },
                rel: { type: 'string', const: '/collection' },
                targetPointer: { type: 'string', const: '/enum' },
                method: { type: 'string', examples: ['GET'] },
                body: { type: 'string', examples: ['request body'] },
                sourcePointer: { type: 'string', examples: ['/'] },
                sourceKeyPointer: { type: 'string', examples: ['/name'] },
                base: { type: 'string', examples: ['http://www.google.com/'] },
                href: { type: 'string', examples: ['api/v1/devices'] },
                validation: { type: 'object' },
              },
              required: ['nodeId', 'type', 'title', 'description', 'placeholder', 'required'],
              additionalProperties: false,
            },
          ],
        },
      },
    },
    required: ['nodeId', 'type', 'items'],
    additionalProperties: false,
  });
});

test('derefSchema schema array', () => {
  const schemas = [
    {
      title: 'struct',
      $ref: 'struct',
    },
  ];

  jsdo.addSchema('schemaArray', schemas);

  expect(jsdo.derefSchema('schemaArray')).toEqual([
    {
      type: 'object',
      properties: {
        nodeId: { type: 'string', examples: ['kdWO-g4oS3GkzrbqDqCZUw'] },
        type: { type: 'string', const: 'array' },
        items: {
          type: 'array',
          items: {
            anyOf: [
              {
                type: 'object',
                properties: {
                  nodeId: {},
                  type: { type: 'string', const: 'array' },
                  title: { type: 'string', examples: ['Connection Information'] },
                  description: {
                    type: 'string',
                    examples: ['This section describes the connection information for a device.'],
                  },
                  items: { type: 'array', items: { anyOf: [{}, {}, {}, {}, {}, {}] } },
                },
                required: ['nodeId', 'type', 'title', 'description', 'items'],
                additionalProperties: false,
              },
              {
                type: 'object',
                properties: {
                  nodeId: { type: 'string', examples: ['kdWO-g4oS3GkzrbqDqCZUw'] },
                  type: { type: 'string', const: 'boolean' },
                  title: { type: 'string', examples: ['Allow timeout'] },
                  description: {
                    type: 'string',
                    examples: ['Is a timeout allowed to pass this step?'],
                  },
                  default: { type: 'boolean', examples: [false] },
                },
                required: ['nodeId', 'type', 'title', 'description'],
                additionalProperties: false,
              },
              {
                type: 'object',
                properties: {
                  nodeId: { type: 'string', examples: ['kdWO-g4oS3GkzrbqDqCZUw'] },
                  type: { type: 'string', const: 'number' },
                  widget: { type: 'string', const: 'updown' },
                  title: { type: 'string', examples: ['Retry attempts'] },
                  description: {
                    type: 'string',
                    examples: ['The number of connection retries before timing out.'],
                  },
                  placeholder: {
                    type: 'string',
                    examples: ['Enter connection retry attempt count'],
                  },
                  required: { type: 'boolean' },
                  validation: { type: 'object' },
                },
                required: ['nodeId', 'type', 'title', 'description', 'placeholder', 'required'],
                additionalProperties: false,
              },
              {
                type: 'object',
                properties: {
                  nodeId: { type: 'string', examples: ['kdWO-g4oS3GkzrbqDqCZUw'] },
                  type: { type: 'string', const: 'string' },
                  title: { type: 'string', examples: ['Device Name'] },
                  description: {
                    type: 'string',
                    examples: ['A unique name of the target device.'],
                  },
                  placeholder: { type: 'string', examples: ["Enter the device's name"] },
                  required: { type: 'boolean' },
                  pattern: { type: 'string', examples: ['regex'] },
                  validation: { type: 'object' },
                },
                required: ['nodeId', 'type', 'title', 'description', 'placeholder', 'required'],
                additionalProperties: false,
              },
              {
                type: 'object',
                properties: {
                  nodeId: { type: 'string', examples: ['kdWO-g4oS3GkzrbqDqCZUw'] },
                  type: { type: 'string', const: 'string' },
                  widget: { type: 'string', const: 'textarea' },
                  title: { type: 'string', examples: ['Device Name'] },
                  description: {
                    type: 'string',
                    examples: ['A unique name of the target device.'],
                  },
                  placeholder: { type: 'string', examples: ["Enter the device's name"] },
                  required: { type: 'boolean' },
                  pattern: { type: 'string', examples: ['regex'] },
                  validation: { type: 'object' },
                },
                required: ['nodeId', 'type', 'title', 'description', 'placeholder', 'required'],
                additionalProperties: false,
              },
              {
                type: 'object',
                properties: {
                  nodeId: { $ref: '#/definitions/nodeId' },
                  type: { type: 'string', const: 'string' },
                  title: { type: 'string', examples: ['Device Name'] },
                  description: {
                    type: 'string',
                    examples: ['A unique name of the target device.'],
                  },
                  placeholder: { type: 'string', examples: ["Enter the device's name"] },
                  required: { type: 'boolean' },
                  enum: { type: 'array', default: [] },
                  binding: { type: 'boolean', default: false },
                  pattern: { type: 'string', examples: ['regex'] },
                  rel: { type: 'string', const: '/collection' },
                  targetPointer: { type: 'string', const: '/enum' },
                  method: { type: 'string', examples: ['GET'] },
                  body: { type: 'string', examples: ['request body'] },
                  sourcePointer: { type: 'string', examples: ['/'] },
                  sourceKeyPointer: { type: 'string', examples: ['/name'] },
                  base: { type: 'string', examples: ['http://www.google.com/'] },
                  href: { type: 'string', examples: ['api/v1/devices'] },
                  validation: { type: 'object' },
                },
                required: ['nodeId', 'type', 'title', 'description', 'placeholder', 'required'],
                additionalProperties: false,
              },
            ],
          },
        },
      },
      required: ['nodeId', 'type', 'items'],
      additionalProperties: false,
    },
  ]);
});
