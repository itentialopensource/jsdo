# JSDO

JSON Schema Dereference Operator, abbreviated to JSDO, is an operator for handling reference properties inside of a JSON Schema.

## Usage Description

JSDO can be used to resolve references inside of JSON Schema documents,
while leaving the implementation deals of how the schemas are retrieved up to the user.
This allows for a wide variety of use-cases,
as the JSON Schema specification leaves it up to the user to define where and how JSON Schema documents are stored.

Currently, JSDO only supports [draft 7 of the JSON Schema specification](https://tools.ietf.org/html/draft-handrews-json-schema-01).

`playground.test.js` provides an example of how to use JSDO when the JSON Schema documents are located on the local file system.

### Primary Features

- Add schemas to an instance of JSDO's schema store.
- Dereference schemas that have previously been added to an instance of JSDO.

### Notice of Unsupported Features

#### Process references that point to a file or remote URL

Supporting files and remote url reference pointers would require JSDO to implement a prescriptive solutions for managing such references.
JSDO is intentionally unopinionated about the implementation details of such file and remote url pointers,
so that users of JSDO can decide how to implement such a feature.

#### Recursive references

Dereferencing recursive references would lead to an infinite loop. For this reason, JSDO does not dereference recursive references.
Instead, if JSDO detects a recursive reference, the reference will be normalized to an empty object.
