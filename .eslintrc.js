module.exports = {
  extends: ['airbnb-base', 'plugin:prettier/recommended', 'plugin:jest/recommended'],
  plugins: ['jest'],
  env: {
    es6: true,
    'jest/globals': true,
    node: true,
  },
  rules: {
    'prettier/prettier': 'error',
    'no-console': ['warn', { allow: ['info', 'warn', 'error'] }],
    'max-len': [
      'warn',
      {
        code: 100,
        tabWidth: 2,
        comments: 100,
        ignoreComments: false,
        ignoreTrailingComments: true,
        ignoreUrls: true,
        ignoreStrings: true,
        ignoreTemplateLiterals: true,
        ignoreRegExpLiterals: true,
      },
    ],
  },
};
