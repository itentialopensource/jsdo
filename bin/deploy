#!/bin/bash
set -e

#----------------------#
# Git Credential Setup #
#----------------------#

git_setup

#-------------------------#
# Checkout Current Branch #
#-------------------------#

# Checkout the branch that was merged into (master, release/x).
CURRENT_BRANCH="$CI_COMMIT_REF_NAME"
echo "Checking out $CURRENT_BRANCH..."
git checkout "$CURRENT_BRANCH"
# Forcefully ensure that the local branch is exactly the same as the remote.
git reset --hard origin/"$CURRENT_BRANCH"
echo "$CURRENT_BRANCH checked out."

#-------------------#
# Get Semver Prefix #
#-------------------#

# Get branch prefix for semver bump, otherwise default to patch.
VERSION="$(git log --format=%s --merges -1|awk -F"'" '{print $2}'|awk -F "/" '{print $1}')"
# Enable case globbing for matching "patch" and "Patch"
shopt -s nocasematch
case "$VERSION" in
  # if PATCH transform to patch
  patch|minor|major) SEMVER="$(echo "$VERSION" | awk '{print tolower($0)}')";;
  *) SEMVER="patch";;
esac
# disable case matching
shopt -s nocasematch
if [ -z "$VERSION" ]; then
  echo "No branch prefix detected. Defaulting to patch."
fi
echo "Semver bump: $SEMVER"

#---------------------#
# Get Current Version #
#---------------------#

CURRENT_VERSION=$(node -p "require('./package.json').version")
echo "Last version: $CURRENT_VERSION"
CURRENT_MAJOR=$(echo "$CURRENT_VERSION"|awk -F'.' '{print $1}')
CURRENT_MINOR=$(echo "$CURRENT_VERSION"|awk -F'.' '{print $2}')
if [[ $CURRENT_VERSION != *"+"* ]]; then
  CURRENT_PATCH=$(echo "$CURRENT_VERSION"|awk -F'.' '{print $3}'|awk -F'-' '{print $1}')
  CURRENT_PRERELEASE=$(echo "$CURRENT_VERSION"|awk -F'-' '{print $2}')
else
  CURRENT_PATCH=$(echo "$CURRENT_VERSION"|awk -F'.' '{print $3}'|awk -F'+' '{print $1}')
  CURRENT_PRERELEASE=$(echo "$CURRENT_VERSION"|awk -F'+' '{print $2}')
fi

#--------------------#
# Create New Version #
#--------------------#

# set version for packages that aren't node modules.
if [ "$SEMVER" == "major" ]; then
  if [[ $CURRENT_VERSION != *"-"* ]]; then
    # no prerelease exists, so we'll bump the major.
    MAJOR="$(bc <<< "$CURRENT_MAJOR"+1)"
  else
    # prerelease exists, let prerelease handle the version bump.
    MAJOR="$CURRENT_MAJOR"
  fi
  MINOR="0"
  PATCH="0"
  PRERELEASE=""
elif [ "$SEMVER" == "minor" ]; then
  MAJOR="$CURRENT_MAJOR"
  MINOR="$(bc <<< "$CURRENT_MINOR"+1)"
  PATCH="0"
  PRERELEASE=""
elif [ "$SEMVER" == "patch" ]; then
  if [[ $CURRENT_BRANCH == *"release"* ]]; then
    if [ -z "$CURRENT_PRERELEASE" ]; then
      MAJOR="$CURRENT_MAJOR"
      MINOR="$CURRENT_MINOR"
      PATCH="$CURRENT_PATCH"
      PRERELEASE="-$(echo "$CURRENT_BRANCH"|awk -F'/' '{print $2".0"}')"
    else
      BUILD_PREFIX="$(echo "$CURRENT_PRERELEASE"|awk -F'.' '{print $1"."$2"."}')"
      BUILD_PATCH="$(bc <<< "$(echo "$CURRENT_PRERELEASE"|awk -F'.' '{print $3}')"+1)"
      MAJOR="$CURRENT_MAJOR"
      MINOR="$CURRENT_MINOR"
      PATCH="$CURRENT_PATCH"
      PRERELEASE="-$BUILD_PREFIX$BUILD_PATCH"
    fi
  else
    MAJOR="$CURRENT_MAJOR"
    MINOR="$CURRENT_MINOR"
    PATCH="$(bc <<< "$CURRENT_PATCH"+1)"
  fi
fi
NEW_VERSION="$MAJOR.$MINOR.$PATCH$PRERELEASE"
echo "New version: $NEW_VERSION"

#---------------------------#
# Version & Release Package #
#---------------------------#
echo "Bumping version..."
# bump version, git tag, commit & then push changes
# using -f to ignore the added release note
if [ -f yarn.lock ]; then
  yarn version "$SEMVER" -m "Updating $SEMVER version to %s. [skip ci]"
else
  npm version -f "$SEMVER" -m "Updating $SEMVER version to %s. [skip ci]" --loglevel=error
fi
# no-verify is used to ignore any pre-push commits that may be used by the project
if git push origin "$CURRENT_BRANCH" --follow-tags --no-verify; then
  echo "Version bumped and pushed."
else
  echo -e "\033[0;31mERROR: ***********************************************************************************"
  echo "ERROR: Failed to push."
  echo -e "ERROR: ***********************************************************************************\033[0m"
  exit 1
fi
# Publish the repo if a deploy runscript exists.
if [[ "$(node -p "typeof (require('./package.json').scripts.deploy) === 'string'")" == true ]]; then
  echo "Running deploy runscript..."
  if [ -f yarn.lock ]; then
    if yarn run deploy; then
      echo "Module has been published."
    else
      echo -e "\033[0;31mERROR: ***********************************************************************************"
      echo "ERROR: Failed to deploy. Unable to run 'yarn run deploy'. Check the output above"
      echo "ERROR: for more information."
      echo "ERROR: Documentation: https://gitlab.com/itentialopensource/argo#available-commands "
      echo -e "ERROR: ***********************************************************************************\033[0m"
      exit 1
    fi
  else
    if npm run deploy; then
      echo "Module has been published."
    else
      echo -e "\033[0;31mERROR: ***********************************************************************************"
      echo "ERROR: Failed to deploy. Unable to run 'npm run deploy'. Check the output above"
      echo "ERROR: for more information."
      echo "ERROR: Documentation: https://gitlab.com/itentialopensource/argo#available-commands"
      echo -e "ERROR: ***********************************************************************************\033[0m"
      exit 1
    fi
  fi
else
  echo "INFO: **************************************************************************************"
  echo "INFO: No 'deploy' runscript detected. This project will not be published to a registry."
  echo "INFO: Documentation: https://gitlab.com/itentialopensource/argo#available-commands"
  echo "INFO: **************************************************************************************"
fi
